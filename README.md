# General Information


# Installing


## Tools required


## Get the code

1. Checkout the FluOCE with SourceTree:
    * *Clone*: clone the repository

      | Paramter | Value |
      		| ------ | ------ |
      | URL | https://USER@bitbucket.hepawash.com/scm/com/communication-infrastructure/lk2001-simulator.git |
      | USER | *your LDAP name* |
      | Destination Path | *local destination for this repository* |
      | Name | *the name of the local repository* |

    * *Repository Settings - Remotes - Paths*: there should be an *origin* path with the following values

      | Paramter | Value |
      		| ------ | ------ |
      | URL | https://USER@bitbucket.hepawash.com/scm/com/communication-infrastructure/lk2001-simulator.git |
      | Host Type | Stash |
      | Username | *your LDAP name* |

    * *Tools - Option - Git*: disable SSL certificate validation