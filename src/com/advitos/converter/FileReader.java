package com.advitos.converter;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.stream.Stream;

public class FileReader {

    private static String path;
    private static final int HEADER_SIZE = 1024;
    private static final String FILE_PATTERN = "glob:*.hwbin";

    public FileReader(String path) {
        FileReader.path = path;
    }

    public List<ByteBuffer> scanFileSystem() {
        List<ByteBuffer> buffers = new ArrayList<>();
        PathMatcher matcher = FileSystems.getDefault().getPathMatcher(FILE_PATTERN);
        Queue<Path> pathQueue = new ConcurrentLinkedQueue<>();
        try {
            Stream<Path> result = Files.walk(Paths.get(path));
            result
                    .filter(Files::isRegularFile)
                    .filter(path -> matcher.matches(path.getFileName()))
                    .forEach(pathQueue::add);
            this.readProcessImageData(buffers, pathQueue);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return buffers;
    }

    void readProcessImageData(List<ByteBuffer> byteBufferList, Queue<Path> pathQueue) {
        while (!pathQueue.isEmpty()) {
            Path path = pathQueue.poll();

            try (BufferedInputStream fileInputStream = new BufferedInputStream(new FileInputStream(path.toFile()))) {

                String file = path.toFile().toString();

                fileInputStream.readNBytes(HEADER_SIZE);
                byte[] intByteArr = new byte[4];

                while (fileInputStream.read(intByteArr) != -1) {

                    int fromByteArray = ByteBuffer.wrap(intByteArr).getInt();
                    byte[] data = new byte[fromByteArray];
                    fileInputStream.read(data);
                    ByteBuffer buffer = ByteBuffer.allocate(intByteArr.length + data.length);
                    buffer.put(intByteArr).put(data);
                    byteBufferList.add(buffer);
                    fileInputStream.read();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
