package com.advitos;

import com.advitos.server.Server;

import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        Server server = new Server();
        try {
            server.startServer(args[0]);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}
