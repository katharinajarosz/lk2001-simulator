package com.advitos.server;

import com.advitos.converter.FileReader;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.util.List;

public class Server {

    public void startServer(String path) throws IOException, InterruptedException {
        int port = 11111;
        ServerSocket serverSocket = new ServerSocket(port);
        Socket client = waitingForClient(serverSocket);
        String message = readMessage(client);
        System.out.println("NACHRICHT VOM CLIENT. " + message);
        streamProcessImageData(client, path);
    }

    Socket waitingForClient(ServerSocket serverSocket) throws IOException {
        return serverSocket.accept();
    }

    void streamProcessImageData(Socket socket, String path) throws IOException, InterruptedException {
        FileReader bb = new FileReader(path);
        List<ByteBuffer> list = bb.scanFileSystem();
        for (ByteBuffer buffer : list) {
            DataOutputStream out = new DataOutputStream(socket.getOutputStream());
            out.write(buffer.array());
            int size = buffer.limit();
            Thread.sleep(250);
        }

    }

    String readMessage(Socket socket) throws IOException {
        BufferedReader bufferedReader =
                new BufferedReader(
                        new InputStreamReader(
                                socket.getInputStream()));
        char[] buffer = new char[200];
        int i = bufferedReader.read(buffer, 0, 200); // blockiert bis Nachricht empfangen
        return new String(buffer, 0, i);
    }

}